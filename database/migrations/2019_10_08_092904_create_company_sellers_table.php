<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanySellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_sellers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon');
            $table->string('name');
            $table->string('company_name_E');
            $table->string('company_name_A');
            $table->integer('category_id');
            $table->text('text_E');
            $table->text('text_A');
            $table->string('email');
            $table->string('phone');
            $table->string('address');
            $table->string('commercial_register');
            $table->string('tax_record');
            $table->string('longitude');
            $table->string('latitude');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_sellers');
    }
}
