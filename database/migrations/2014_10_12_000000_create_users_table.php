<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->unique();
            $table->string('user_type');
            $table->string('image');
            $table->string('commercial_register');
            $table->string('tax_record');
            $table->integer('city_id');
            $table->string('verify_code');
            $table->boolean('user_status');
            $table->boolean('status');
            $table->string('jwt_token');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->string('firebase_token');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
