<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTakeDownContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('take_down_containers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('photo');
            $table->integer('container_id');
            $table->integer('user_id');
            $table->text('note_E');
            $table->text('note_A');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('take_down_containers');
    }
}
