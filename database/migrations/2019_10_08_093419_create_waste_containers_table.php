<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWasteContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waste_containers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_E');
            $table->string('name_A');
            $table->integer('company_id');
            $table->string('price');
            $table->string('distance');
            $table->text('description_E');
            $table->text('description_A');
            $table->date('date');
            $table->string('month');
            $table->string('days');
            $table->string('total');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waste_containers');
    }
}
