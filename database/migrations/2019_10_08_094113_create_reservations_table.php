<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('icon');
            $table->integer('company_id');
            $table->integer('container_id');
            $table->integer('company_id');
            $table->string('order_number');
            $table->date('date_from');
            $table->date('date_to');
            $table->string('total');
            $table->boolean('status');
            $table->string('payment_info');
            $table->string('days');
            $table->string('price_per_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
