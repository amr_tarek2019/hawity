<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" type="image/x-icon">
    <link rel="shortcut icon" href="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" type="image/x-icon">
    <title>Hawity</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/fontawesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/icofont.css') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/themify.css') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/flag-icon.css') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/feather-icon.css') }}">
    <!-- Plugins css start-->
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('assets/dashboard/css/light-1.css') }}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/dashboard/css/responsive.css') }}">
</head>
<body>
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper">
    <div class="container-fluid p-0">
        <!-- login page start-->
        <div class="authentication-main">
            <div class="row">
                <div class="col-md-12">
                    <div class="auth-innerright">
                        <div class="authentication-box">
                            <div class="text-center"><img src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt=""></div>
                            <div class="card mt-4">
                                <div class="card-body">
                                    <div class="text-center">
                                        <h4>LOGIN</h4>
                                        <h6>Enter your Username and Password </h6>
                                    </div>
                                    <form class="theme-form" method="POST" action="{{route('admin.login')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="col-form-label pt-0">Your Email</label>
                                            <input value="{{old('email')}}" class="form-control{{$errors->has('email')?'is-invalid':''}}" id="email" name="email" type="email" required="">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label">Password</label>
                                            <input class="form-control{{$errors->has('password')?'is-invalid':''}}" name="password" id="password" type="password" required="">
                                        </div>
                                        <div class="checkbox p-0">
                                            <input id="checkbox1" name="remember" type="checkbox" {{old('remember')?'checked':''}}>
                                            <label for="checkbox1">Remember me</label>
                                        </div>
                                        <div class="form-group form-row mt-3 mb-0">
                                            <button class="btn btn-primary btn-block" type="submit">Login</button>
                                        </div>


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- login page end-->
    </div>
</div>

<footer id="footer" style="
    margin-top: -20px;
">
    <div class="container" style="max-height: 60px;
">

        <!-- footer socials -->
        <div class="row">

            <div class="footer_socials col-sm-12 text-center">

                <div class="contact_icons">
                    <!--<ul class="contact_socials clearfix animated" data-animation="fadeIn" data-animation-delay="600">-->
                    <!--&lt;!&ndash; social icons &ndash;&gt;-->
                    <!--<li><a class="ukie_social" href="fortyfour.marketing@gmail.com"><i class="fa fa-envelope-square"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="https://www.facebook.com/44.marketing.agency"><i class="fa fa-facebook"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="https://plus.google.com/u/3/108749791564217391732"><i class="fa fa-google-plus"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="https://www.linkedin.com/company/44agency/"><i class="fa fa-linkedin"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="https://www.youtube.com/channel/UCdGsB5zHWEPHtf3QeebtSMQ/featured?view_as=subscriber"><i class="fa fa-youtube"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="https://www.instagram.com/44.agency"><i class="fa fa-instagram"></i></a></li>-->
                    <!--&lt;!&ndash;-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-twitter"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-behance"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-dribbble"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-pinterest"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-digg"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-deviantart"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-delicious"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-dropbox"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-skype"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-tumblr"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-vimeo-square"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-flickr"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-github-alt"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-renren"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-vk"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-xing"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-weibo"></i></a></li>-->
                    <!--<li><a class="ukie_social" href="#"><i class="fa fa-rss"></i></a></li>-->
                    <!--&ndash;&gt;-->
                    <!--</ul>-->
                </div>

                <div class=“site-copyright”>
                    <img src="{{ asset('assets/coming/assets/img/grandandroid.png') }}" style="
    max-width: 100px;
">
                </div>
            </div>

        </div>
        <!-- end footer socials -->

    </div>
    <!-- end container -->
</footer>
<!-- latest jquery-->
<script src="{{ asset('assets/dashboard/js/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('assets/dashboard/js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/bootstrap/bootstrap.js') }}"></script>
<!-- feather icon js-->
<script src="{{ asset('assets/dashboard/js/icons/feather-icon/feather.min.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/icons/feather-icon/feather-icon.js') }}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('assets/dashboard/js/sidebar-menu.js') }}"></script>
<script src="{{ asset('assets/dashboard/js/config.js') }}"></script>
<!-- Plugins JS start-->
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('assets/dashboard/js/script.js') }}"></script>
<!-- Plugin used-->
</body>
</html>