@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Show Company</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Company</li>
                                <li class="breadcrumb-item active">Show Company</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">

                    <div class="card">
                        <div class="card-header">
                            <h5>Show Company</h5>
                        </div>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" value="{{$companySeller->name}}" type="text" name="name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Company Name Arabic</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" value="{{$companySeller->company_name_A}}" name="company_name_A" placeholder="Company Name Arabic" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Company Name English</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="company_name_E" value="{{$companySeller->company_name_E}}" placeholder="Company Name English" type="text">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlSelect9">Category select</label>
                                            <select class="form-control digits" name="category">
                                                @foreach($categories as $category)
                                                    <option {{ $category->id == $companySeller->category->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name_E }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 col-form-label">Text English</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="text_E"  rows="5" cols="5" placeholder="Text English">{{$companySeller->text_E}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 col-form-label">Text Arabic</label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="text_A"   rows="5" cols="5" placeholder="Text Arabic">{{$companySeller->text_A}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Email address</label>
                                            <input class="form-control" type="email" value="{{$companySeller->email}}" name="email" placeholder="name@example.com">
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Telephone</label>
                                            <div class="col-sm-9">
                                                <input class="form-control m-input digits" name="phone" value="{{$companySeller->phone}}" type="tel" placeholder="91-(999)-999-999">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Address</label>
                                            <div class="col-sm-9">
                                                <input class="form-control m-input digits" name="address" value="{{$companySeller->address}}" type="text" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Commercial Register</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="number" name="commercial_register" value="{{$companySeller->commercial_register}}" placeholder="Commercial Register">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Tax Record</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="number" name="tax_record" value="{{$companySeller->tax_record}}" placeholder="Tax Record">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Uploaded Icon</label>
                                            <div class="col-sm-9">
                                                {{--<input class="form-control" type="file" name="icon" data-original-title="" title="">--}}
                                                <img class="img-responsive img-thumbnail" src="{{ asset($companySeller->icon) }}" style="height: 100px; width: 100px" alt="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <a href="{{route('company.index')}}" class="btn btn-light">
                                        back
                                    </a>
                                </div>
                            </div>

                    </div>

                </div>
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')

                    <div class="card">
                        <div class="card-header">
                            <h5>Company Orders</h5>
                            <h6>this company have {{$companyOrdersCount}} order </h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <div id="advance-1_wrapper" class="dataTables_wrapper">

                                    <table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Waste Container</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Order</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Created At</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Action</th>

                                        </thead>
                                        <tbody>

                                        @foreach($companyOrders as $key=>$order)
                                            <tr role="row" class="odd">
                                                <td class="sorting_1">{{ $key + 1 }}</td>
                                                <td>{{ $order->user['name'] }}</td>
                                                <td>{{ $order->wasteContainer['name_E']}}
                                                    distance :{{$order->wasteContainer['distance']}} yard</td>
                                                <td>{{ $order->reservation['order_number'] }}</td>
                                                <td>
                                                    <span class="label label-info">Confirmed</span>
                                                </td>
                                                <td>{{ $order->created_at }}</td>
                                                <td>

                                                    <a href="{{ route('order.show',$order->id) }}" class="btn btn-info btn-sm"><i class="material-icons">details</i></a>

                                                </td>
                                            </tr>
                                        @endforeach

                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Waste Container</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Order</th>
                                            <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Status</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Created At</th>
                                            <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Action</th>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection