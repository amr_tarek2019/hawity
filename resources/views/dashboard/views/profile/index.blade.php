@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit Profile</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Users</li>
                                <li class="breadcrumb-item active">Edit Profile</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')

                        <form class="card" method="POST" action="{{ route('profile.update',Auth::user()->id) }}" enctype="multipart/form-data">
                            <input name="_method" type="hidden" value="PATCH">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">Edit Profile</h4>
                            </div>
                            <div class="card-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="form-group">
                                            <label class="form-label">User Name</label>
                                            <input class="form-control" type="text" name="name" value="{{ Auth::user()->name }}">
                                        </div>
                                    </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input class="form-control" type="email" name="email" value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                                {{--<div class="col-sm-6 col-md-6">--}}
                                {{--<div class="form-group">--}}
                                {{--<img class="align-self-center pull-left img-50 rounded-circle"--}}
                                     {{--src="{{ asset( Auth::user()->image ) }}">--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<br>  <br>  <br>--}}
                                {{--<div class="col-sm-6 col-md-6">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<input class="form-control" name="image" type="file">--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Password</label>
                                        <input class="form-control" type="password" name="password" placeholder="Password">
                                    </div>
                                </div>



                            </div>
                            <div class="card-footer text-left">
                                <button class="btn btn-primary" type="submit">Update Profile</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection