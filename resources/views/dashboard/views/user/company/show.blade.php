@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit User</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Users</li>
                                <li class="breadcrumb-item active">Show Company Data</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')

                            <div class="card-header">
                                <h4 class="card-title mb-0">Show Company Data</h4>
                            </div>
                            <div class="card-body">
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Name</label>
                                        <input class="form-control" type="text" name="name" value="{{$user->name}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Email</label>
                                        <input class="form-control" type="email" name="email" value="{{$user->email}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Phone</label>
                                        <input class="form-control" type="number" name="phone" value="{{$user->phone}}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Commercial Register</label>
                                        <input class="form-control" type="number" name="commercial_register" value="{{$user->commercial_register}}">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">Tax Record</label>
                                        <input class="form-control" type="number" name="tax_record" value="{{$user->tax_record}}">
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6">
                                    <div class="form-group">
                                        <label class="form-label">City</label>
                                        <select class="form-control" name="city">
                                            @foreach($cities as $city)
                                                <option {{ $city->id == $user->city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name_E }}</option>
                                            @endforeach
                                        </select>                                    </div>
                                </div>

                            </div>
                            <div class="card-footer text-left">
                                <a href="{{route('user.company.index')}}" class="btn btn-primary">back</a>
                            </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>



@endsection