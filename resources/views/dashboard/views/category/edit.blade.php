@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit category</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Category</li>
                                <li class="breadcrumb-item active">Edit Category</li>
                            </ol>
                        </div>
                    </div>
                    <!-- Bookmark Start-->

                    <!-- Bookmark Ends-->
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')

                    <div class="card">

                        <div class="card-body">
                            <form class="needs-validation" novalidate=""  method="POST" action="{{ route('category.update',$category->id) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom01">Arabic Name</label>
                                        <input class="form-control" id="name_A" type="text" value="{{$category->name_A}}" name="name_A" placeholder="Arabic Name" required="" data-original-title="" title="">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="col-md-6 mb-3">
                                        <label for="validationCustom03">English Name</label>
                                        <input class="form-control" value="{{$category->name_E}}" id="name_E" name="name_E" type="text" placeholder="English Name" required="" data-original-title="" title="">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-9">
                                <img class="img-responsive img-thumbnail" src="{{ asset($category->image) }}" style="height: 100px; width: 100px" alt="">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-md-6 mb-3">Upload File</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="file" name="image" data-original-title="" title="">
                                    </div>
                                </div>

                                <button class="btn btn-primary" type="submit" data-original-title="" title="">Submit form</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection