@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Show Order</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Order</li>
                                <li class="breadcrumb-item active">Show Order</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        {{--<!-- Container-fluid starts-->--}}
        {{--<div class="container-fluid">--}}
            {{--<div class="row">--}}
                {{--<div class="col-sm-12">--}}
                    {{--<div class="card">--}}

                            {{--<div class="card-body">--}}
                                {{--<div class="row">--}}
                                    {{--<div class="col">--}}
                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">User</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->user['name'] }}">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Waste Container</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->wasteContainer['name_E']}} - distance :{{$reservation->wasteContainer['distance']}} yard">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Waste Container image</label>--}}
                                            {{--<img class="img-responsive img-thumbnail" src="{{ asset($reservation->wasteContainer->image) }}" style="height: 100px; width: 100px" alt="">--}}

                                        {{--</div>--}}

                                         {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Order Number</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->order_number }}">--}}
                                        {{--</div>--}}
                                        {{----}}
                                           {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Date From</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->date_from }}">--}}
                                        {{--</div>--}}
                                        {{----}}
                                              {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Date To</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->date_to }}">--}}
                                        {{--</div>--}}
                                        {{----}}
                                               {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Days</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->days }}">--}}
                                        {{--</div>--}}
                                        {{----}}
                                                 {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Total</label>--}}
                                            {{--<input class="form-control" id="title" type="text" value="{{ $reservation->total }}$">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">status</label>--}}
                                            {{--<input class="form-control" id="status" type="text" value="@if ($reservation->status == true) Confirmed @else not Confirmed @endif">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Payment Info</label>--}}
                                            {{--<input class="form-control" id="payment_info" type="text" value="@if ($reservation->status == true) cash @else Online @endif">--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">Distance</label>--}}
                                            {{--<input class="form-control" id="distance" type="text" value="{{ $reservation->wasteContainer->distance }} yard">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-md-9 mb-3">--}}
                                            {{--<label for="validationTooltip01">company name</label>--}}
                                            {{--<input class="form-control" id="companySeller" type="text" value="{{ $reservation->companySeller->company_name_E }}">--}}
                                        {{--</div>--}}
                                        {{--<div class="form-group row">--}}
                                            {{--<label class="col-sm-3 col-form-label">Uploaded Image</label>--}}
                                            {{--<div class="col-sm-9">--}}
                                                {{--<input class="form-control" type="file" name="icon" data-original-title="" title="">--}}
                                                {{--<img class="img-responsive img-thumbnail" src="{{ asset($reservation->companySeller->icon) }}" style="height: 100px; width: 100px" alt="">--}}
                                            {{--</div>--}}
                                        {{--</div>--}}

                                        {{----}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="card-footer">--}}
                                {{--<div class="col-sm-9 offset-sm-3">--}}
                                    {{--<a href="{{route('reservation.index')}}" class="btn btn-light">--}}
                                        {{--back--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}

                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Container-fluid Ends-->--}}

        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-xl-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>User Data</h5>
                                </div>
                                <div class="card-body">

                                        <div class="form-group">
                                            <label class="col-form-label pt-0" for="exampleInputEmail1">User Name</label>
                                            <input class="form-control" id="title" type="text" value="{{ $order->user['name'] }}" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">User Email</label>
                                            <input class="form-control" id="title" type="text" value="{{ $order->user['email'] }}" readonly="readonly">
                                        </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone</label>
                                        <input class="form-control" id="title" type="text" value="{{ $order->user['phone'] }}" readonly="readonly">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Company Data</h5>
                                </div>
                                <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label" for="inputEmail3">company Name</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" id="companySeller" type="text" value="{{ $order->companySeller->company_name_E }}" readonly="readonly">
                                            </div>
                                        </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label" for="inputEmail3">company Image</label>
                                        <div class="col-sm-9">
                                            <img class="img-responsive img-thumbnail" src="{{ asset($order->companySeller->icon) }}" style="height: 100px; width: 100px" alt="">
                                        </div>
                                    </div>
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">company name</label>--}}
                                    {{--<input class="form-control" id="companySeller" type="text" value="{{ $reservation->companySeller->company_name_E }}">--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group row">--}}
                                    {{--<label class="col-sm-3 col-form-label">Uploaded Image</label>--}}
                                    {{--<div class="col-sm-9">--}}
                                    {{--<input class="form-control" type="file" name="icon" data-original-title="" title="">--}}
                                    {{--<img class="img-responsive img-thumbnail" src="{{ asset($reservation->companySeller->icon) }}" style="height: 100px; width: 100px" alt="">--}}
                                    {{--</div>--}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-xl-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Waste Container Data</h5>
                                </div>
                                <div class="card-body">
                                    <div class="col-md-9 mb-3">
                                    <label for="validationTooltip01">Waste Container</label>
                                    <input class="form-control" id="title" type="text" value="{{ $order->wasteContainer['name_E']}} - distance :{{$order->wasteContainer['distance']}} yard" readonly="readonly">
                                    </div>

                                    <div class="col-md-9 mb-3">
                                    <label for="validationTooltip01">Waste Container image</label>
                                    <img class="img-responsive img-thumbnail" src="{{ asset($order->wasteContainer->image) }}" style="height: 100px; width: 100px" alt="">
                                    </div>
                                    <div class="col-md-9 mb-3">
                                    <label for="validationTooltip01">Distance</label>
                                    <input class="form-control" id="distance" type="text" value="{{ $order->wasteContainer->distance }} yard" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Order Data</h5>
                                </div>
                                <div class="card-body">
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlineUsername">Order Number</label>
                                            <input class="form-control" id="title" type="text" value="{{ $order->order_number }}" readonly="readonly">
                                        </div>
                                        <div class="form-group">
                                            <label class="col-form-label" for="inputInlinePassword">Date From</label>
                                            <input class="form-control" id="title" type="text" value="{{ $order->date_from }}" readonly="readonly">
                                        </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputInlinePassword">Date To</label>
                                        <input class="form-control" id="title" type="text" value="{{ $order->date_to }}" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputInlinePassword">Days</label>
                                        <input class="form-control" id="title" type="text" value="{{ $order->days }}" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputInlinePassword">Total</label>
                                        <input class="form-control" id="title" type="text" value="{{ $order->total }}" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputInlinePassword">status</label>
                                        <input class="form-control" id="status" type="text" value="@if ($order->status == true) Confirmed @else not Confirmed @endif" readonly="readonly">
                                    </div>
                                    <div class="form-group">
                                        <label class="col-form-label" for="inputInlinePassword">status</label>
                                        <input class="form-control" id="payment_info" type="text" value="@if ($order->status == true) cash @else Online @endif" readonly="readonly">
                                    </div>
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Order Number</label>--}}
                                    {{--<input class="form-control" id="title" type="text" value="{{ $reservation->order_number }}">--}}
                                    {{--</div>--}}
                                    {{----}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Date From</label>--}}
                                    {{--<input class="form-control" id="title" type="text" value="{{ $reservation->date_from }}">--}}
                                    {{--</div>--}}
                                    {{----}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Date To</label>--}}
                                    {{--<input class="form-control" id="title" type="text" value="{{ $reservation->date_to }}">--}}
                                    {{--</div>--}}
                                    {{----}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Days</label>--}}
                                    {{--<input class="form-control" id="title" type="text" value="{{ $reservation->days }}">--}}
                                    {{--</div>--}}
                                    {{----}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Total</label>--}}
                                    {{--<input class="form-control" id="title" type="text" value="{{ $reservation->total }}$">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">status</label>--}}
                                    {{--<input class="form-control" id="status" type="text" value="@if ($reservation->status == true) Confirmed @else not Confirmed @endif">--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-9 mb-3">--}}
                                    {{--<label for="validationTooltip01">Payment Info</label>--}}
                                    {{--<input class="form-control" id="payment_info" type="text" value="@if ($reservation->status == true) cash @else Online @endif">--}}
                                    {{--</div>--}}

                                </div>
                            </div>
                        </div>
                        <a style="
    margin-left: -80px;
    margin-bottom: 15px;"
                           href="{{route('order.invoice.show',$order->id)}}" class="btn btn-primary buttons-pdf buttons-html5" tabindex="0" aria-controls="export-button"><span>Download As PDF</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection