@extends('dashboard.layouts.master')
@section('content')
<div class="page-body">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>Dashboard</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-8 xl-100">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="chart-widget-dashboard">
                                    <div class="media">
                                        <div class="media-body">
                                            <h5 class="mt-0 mb-0 f-w-600"><span class="counter">{{$countOrders}}</span></h5>
                                            <p>Total Orders</p>
                                        </div><i data-feather="tag"></i>
                                    </div>
                                    <div class="dashboard-chart-container">
                                        <div class="small-chart-gradient-1"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="chart-widget-dashboard">
                                    <div class="media">
                                        <div class="media-body">
                                            <h5 class="mt-0 mb-0 f-w-600"><span class="counter">{{$countCompany}}</span></h5>
                                            <p>Total Companies</p>
                                        </div><i data-feather="company"></i>
                                    </div>
                                    <div class="dashboard-chart-container">
                                        <div class="small-chart-gradient-2"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="chart-widget-dashboard">
                                    <div class="media">
                                        <div class="media-body">
                                            <h5 class="mt-0 mb-0 f-w-600"><i data-feather="dollar-sign"></i><span class="counter">{{$countContainers}}</span></h5>
                                            <p>Total Containers</p>
                                        </div><i data-feather="sun"></i>
                                    </div>
                                    <div class="dashboard-chart-container">
                                        <div class="small-chart-gradient-3"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
             <div class="col-xl-4 xl-100">
                <div class="card">
                    <div class="card-body">
                        <h5>Last 5 Suggestions</h5>

                        <div class="table-responsive">
                            <div id="advance-1_wrapper" class="dataTables_wrapper">

                                <table class="display dataTable" id="advance-1" role="grid" aria-describedby="advance-1_info">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User Name</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User Email</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Title</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 150px;">Suggestions Or Problem</th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Created At</th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 200px;">Actions</th>
                                    </thead>
                                    <tbody>
                                 

                                    @foreach($sugestions as $key=>$suggestion)

                                        <tr role="row" class="odd">
                                            <td class="sorting_1">{{ $key + 1 }}</td>
                                            <td>{{ $suggestion->user['name'] }}</td>
                                            <td>{{ $suggestion->user['email'] }}</td>
                                            <td>{{ $suggestion->title }}</td>
                                            <td>{{ $suggestion->suggestion }}</td>
                                            <td>{{ $suggestion->created_at }}</td>
                                            <td>
                                                <a href="{{ route('suggestion.show',$suggestion->id) }}" class="btn btn-info active"><i class="material-icons">show</i></a>

                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">#</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User Name</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">User Email</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 100px;">Title</th>
                                        <th class="sorting_asc" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 150px;">Suggestions Or Problem</th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">Created At</th>
                                        <th class="sorting" tabindex="0" aria-controls="advance-1" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 200px;">Actions</th>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6">
                <div class="card height-equal">
                    <div class="card-header card-header-border">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>New Companies</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="new-users">
                            @php
                                $companies = \App\CompanySeller::orderBy('id', 'desc')->take(4)->get();
                            @endphp
                            @foreach($companies as $company)
                               <div class="media">
                                <img class="rounded-circle image-radius m-r-15" src="{{ asset($company->icon) }}" alt="">
                                <div class="media-body">
                                    <h6 class="mb-0 f-w-700">{{$company->company_name_E}}</h6>
                                    <p>{{$company->address}}</p>

                                </div>
                                <div style="margin-top: 10px;">
                                    <a href="{{ route('company.show',$company->id) }}" class="btn btn-info btn-sm"><i class="material-icons">details</i></a>
                                </div>
                            </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card height-equal">
                    <div class="card-header card-header-border">
                        <div class="row">
                            <div class="col-sm-7">
                                <h5>Last 5 Users</h5>
                            </div>
                        </div>
                    </div>
                    @php
                        $users = \App\User::where('user_type','company')->orWhere('user_type','individual')->orderBy('id', 'desc')->take(5)->get();
                    @endphp

                    <div class="card-body recent-notification">
                        @foreach($users as $user)
                        <div class="media">
                            <h6>{{$user->user_type}}</h6>
                            <div class="media-body"><span>{{$user->email}}</span>
                                <p class="f-12">{{$user->name}}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
              <div class="col-xl-6">
                <div class="card height-equal">
                    <div class="card-header card-header-border">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Last 5 Orders</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="new-users">
                            @php
                                $orders = \App\OrderDetail::orderBy('id', 'desc')->take(5)->get();
                            @endphp
                            @foreach($orders as $order)
                                <div class="media">
                                    <img class="rounded-circle image-radius m-r-15" src="{{ asset($order->wasteContainer->image) }}" alt="">
                                    <div class="media-body">
                                        <h6 class="mb-0 f-w-700">{{ $order->user['name'] }}</h6>
                                        <h6 class="mb-0 f-w-700">{{ $order->companySeller['company_name_E'] }}</h6>
                                    </div>
                                    <div style="margin-top: 10px;">
                                    <a href="{{ route('order.show',$order->id) }}" class="btn btn-info btn-sm"><i class="material-icons">details</i></a>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card height-equal">
                    <div class="card-header card-header-border">
                        <div class="row">
                            <div class="col-sm-6">
                                <h5>Last 4 Containers</h5>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="new-users">
                            @php
                                $containers = \App\WasteContainer::orderBy('id', 'desc')->take(4)->get();
                            @endphp
                            @foreach($containers as $container)
                                <div class="media">
                                    <img class="rounded-circle image-radius m-r-15" src="{{ asset($container->image) }}" alt="">
                                    <div class="media-body">
                                        <h6 class="mb-0 f-w-700">{{$container->name_E}}</h6>
                                    </div>
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->
</div>
@endsection