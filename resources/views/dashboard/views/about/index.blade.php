@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>About</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Settings</li>
                                <li class="breadcrumb-item active">About</li>
                            </ol>
                        </div>
                    </div>
                    <!-- Bookmark Start-->

                    <!-- Bookmark Ends-->
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')

                    <div class="card">

                        <div class="card-body">
                            <form class="needs-validation" novalidate=""  method="POST" action="{{ route('about.update') }}" enctype="multipart/form-data">
                                @csrf
                                <!--<div class="form-row">-->
                                <!--    <div class="col-md-6 mb-3">-->
                                <!--        <label for="validationCustom01">Arabic About</label>-->
                                <!--        <input class="form-control" id="about_A" type="text" name="about_A" placeholder="Arabic About" required="" data-original-title="" title=""-->
                                <!--         value="{{$setting->about_A}}">-->
                                <!--    </div>-->

                                <!--</div>-->
                                <br>
                                <div class="row">
                        <div class="col">
                          <div class="form-group mb-0">
                            <label for="exampleFormControlTextarea19"> Arabic About</label>
                            <textarea class="form-control input-air-primary" id="about_A" name="about_A" rows="3">{{$setting->about_A}}</textarea>
                          </div>
                        </div>
                      </div>
                                <!--<div class="form-row">-->
                                <!--    <div class="col-md-6 mb-3">-->
                                <!--        <label for="validationCustom03">English About</label>-->
                                <!--        <textarea class="form-control" id="about_E" name="about_E" type="text" placeholder="English About" required="" data-original-title="" title=""-->
                                <!--       >{{$setting->about_E}}</textarea>-->
                                <!--    </div>-->
                                <!--</div>-->
                                <br>
<div class="row">
                        <div class="col">
                          <div class="form-group mb-0">
                            <label for="exampleFormControlTextarea19">English About</label>
                            <textarea class="form-control input-air-primary" id="about_E" name="about_E" rows="3">{{$setting->about_E}}</textarea>
                          </div>
                        </div>
                      </div>
                                <div class="form-group row">
                                    <label class="col-md-6 mb-3">Upload File</label>
                                    <div class="col-sm-9">
                                        <input class="form-control" type="file" name="icons" data-original-title="" title="">
                                    </div>
                                </div>

                                <button class="btn btn-primary" type="submit" data-original-title="" title="">Submit form</button>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection