<div class="page-sidebar">
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper" style="
    margin-left: 80px;
"><a href="{{route('dashboard')}}"><img src="{{ asset('assets/coming/assets/img/favicons/Group 1188.png') }}" alt=""></a></div>
    </div>
    <div class="sidebar custom-scrollbar">
    
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i><span>Dashboard</span></a>
            </li>

          <li><a class="sidebar-header" href="#"><i data-feather="users"></i><span>Users</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    @if(Auth::user()->user_type=='admin')
                    <li><a href="{{route('user.admin.index')}}"><i class="fa fa-circle"></i>Admin</a></li>
                    <li><a href="{{route('user.member.index')}}"><i class="fa fa-circle"></i>Member</a></li>
                    @endif
        
                    <li><a href="{{route('user.company.index')}}"><i class="fa fa-circle"></i>Company</a></li>
                    <li><a href="{{route('user.individual.index')}}"><i class="fa fa-circle"></i>individual</a></li>


                </ul>
            </li>
   <li><a class="sidebar-header" href="index.html#">
                    <i data-feather="activity"></i><span>Analysis</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('user.analysis.index')}}"><i class="fa fa-circle"></i>Users</a></li>
                    <li><a href="{{route('order.analysis.index')}}"><i class="fa fa-circle"></i>Orders</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="{{route('category.index')}}"><i data-feather="menu"></i><span>Categories</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('company.index')}}"><i class="icofont icofont-company"></i><span>Company</span></a>

            <li><a class="sidebar-header" href="{{route('container.index')}}"><i data-feather="trash"></i><span> Containers</span></a></li>
            <li><a class="sidebar-header" href="index.html#">
                    <i data-feather="tag"></i><span>Booking</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('reservation.index')}}"><i class="fa fa-circle"></i>Reservations</a></li>
                    <li><a href="{{route('order.index')}}"><i class="fa fa-circle"></i>Orders</a></li>
                </ul>
            </li>
   <li><a class="sidebar-header" href="index.html#">
           <i data-feather="settings"></i><span>Settings</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                  <li><a href="{{route('about.index')}}"><i class="fa fa-circle"></i>About</a></li>
                  <li><a href="{{route('privacy.index')}}"><i class="fa fa-circle"></i>Privacy</a></li>
                </ul>
              </li>
            <li><a class="sidebar-header" href="{{route('suggestion.index')}}"><i data-feather="mail"></i><span> Suggestions</span></a></li>
                       <li><a class="sidebar-header" href="{{route('notification.index')}}"><i data-feather="bell"></i><span>Push Notifications</span></a></li>


        </ul>
    </div>
</div>