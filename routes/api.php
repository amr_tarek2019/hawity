<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register/company', 'API\AuthController@registerCompany');
Route::post('register/user', 'API\AuthController@registerUser');
Route::post('login','API\AuthController@login');
Route::post('forgot/password', 'API\AuthController@forgotPassword');
Route::post('code/activation', 'API\AuthController@activateCode');
Route::post('reset/password', 'API\AuthController@resetPassword');
Route::post('company/seller/register', 'API\CompanySellerController@CompanySellerRegister');
Route::get('categories','API\CategoriesController@index');
Route::post('companies/list', 'API\CompanySellerController@index');
Route::post('waste/containers/list', 'API\WasteContainerController@index');
Route::get('waste/containers','API\WasteContainerController@show');
Route::get('reservations','API\ReservationController@indexBooking');
Route::get('orders','API\ReservationController@indexHistory');
Route::post('profile', 'API\ProfileController@index');
Route::post('profile/update', 'API\ProfileController@update');
Route::get('about', 'API\SettingsController@indexAbout');
Route::get('privacy', 'API\SettingsController@indexPrivacy');
Route::post('suggestions', 'API\SuggestionsController@store');
Route::get('get/order','API\ReservationController@editBooking');
Route::post('reservation','API\ReservationController@reserve');
Route::post('reservation/delete','API\ReservationController@destroy');
Route::post('reservation/update','API\ReservationController@update');
Route::post('takedown/container','API\TakeDownContainerController@store');
Route::post('profile/password/update', 'API\ProfileController@updatePassword');
Route::get('notifications', 'API\NotificationsController@index');
Route::get('order/details', 'API\OrderDetailsController@show');

Route::get('container/details', 'API\WasteContainerController@containerDetails');

Route::post('order/delete','API\OrderDetailsController@destroy');

//get user reservations
Route::get('user/reservation', 'API\ReservationController@IndexReservation');

//update reservation
Route::post('confirm/reservation','API\ReservationController@confirmReservation');




//get cities
Route::get('cities', 'API\CityController@index');

//insert location
Route::post('location', 'API\AuthController@location');


//update firebase token
Route::post('firebase/token/update', 'API\NotificationsController@updateToken');
