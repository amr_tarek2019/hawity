<?php

namespace App\Http\Controllers\Dashboard;

use App\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WasteContainer;
use App\CompanySeller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $orders = Reservation::where('status',1)->get();
        return view('dashboard.views.order.index',compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
           $order = Reservation::find($id);
        return view('dashboard.views.order.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $containers=WasteContainer::all();
         $companies=CompanySeller::all();
         $order = Reservation::find($id);
        return view('dashboard.views.order.edit',compact('order','companies','containers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    $this->validate($request, [
        'container' => 'required',
        'company' => 'required',
        'date_from'=>'required',

    ]);

    // $input = $request->all();

    // $reservation->fill($input)->save();
        $order = Reservation::find($id);

        $order->container_id = $request->container;
     $order->company_id = $request->company;
     $order->date_from = $request->date_from;
    //  dd($request->all());

          $order->save();

    return redirect()->route('order.index')->with('successMsg','Order Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order= Reservation::find($id);
        $order->delete();
        return redirect()->route('order.index')->with('successMsg','Order Successfully Deleted');

    }

    function generate_order_pdf($id) {
        $order = Reservation::find($id);
        ini_set('memory_limit', '-1');  //increase size as you need
        $pdf = PDF::loadView('dashboard.views.invoices.order_invoice',compact('order'));
        return $pdf->download('order_invoice.pdf');
    }
}
