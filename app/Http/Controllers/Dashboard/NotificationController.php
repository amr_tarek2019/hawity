<?php

namespace App\Http\Controllers\Dashboard;

use App\Notification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifications = Notification::all();
        return view('dashboard.views.notification.index',compact('notifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $individuals,User $companies)
    {
        //$users = User::where('user_type', 'company')->orWhere('user_type', 'individual')->get();
        foreach ($individuals as $individual){
            $individual->user_type = 'individual';
        }
        foreach ($companies as $company){
            $company->user_type = 'company';
        }

        return view('dashboard.views.notification.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        if (Auth::guard('admin')->user()->can('send_user_notification')){
        $validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $user_type = explode('|', $request->name);
        if ($user_type['0'] == 'All_individuals'){
            $users_token = User::pluck('firebase_token')->toArray();
            $users = User::get();
            send_to_user($users_token,$request->notification ,'7',"");
            foreach ($users as $user){
                UserNotification::create([
                    'user_id' => $user->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->notification
                ]);
            }
        }elseif ($user_type['0'] == 'All_companies'){
            $companies_token = User::pluck('firebase_token')->toArray();
            $companies = Worker::get();
            send_to_worker($companies_token,$request->notification ,'7',"");
            foreach ($companies as $company){
                WorkerNotification::create([
                    'worker_id' => $company->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->notification
                ]);
            }
        }else{
            if ($user_type['1'] == 'company'){
                $user = User::where('id',$user_type['company'])->first();
                send([$user->firebase_token],$request->notification ,'7',"");
                Notification::create([
                    'user_id' => $user->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->notification
                ]);
            }elseif ($user_type['1'] == 'individual'){
                $individual = User::where('id',$user_type['individual'])->first();
                send([$individual->firebase_token],$request->notification ,'7',"");
                Notification::create([
                    'user_id' => $individual->id,
                    'title' => 'رسالة من مدير النظام',
                    'body' => $request->notification
                ]);
            }
        }
        return redirect()->route('notification.index')->with('successMsg','User Notification Sended');

//        }
//        else{
//            session()->flash('error', 'عذراً لا يمكنك الدخول لهذة الصفحة');
//            return redirect()->back();
//        }
    }






    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notification = Notification::find($id);
        $users = User::where('user_type', 'company')->orWhere('user_type', 'individual')->get();
        return view('dashboard.views.notification.edit',compact('users','notification'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notification = Notification::find($id);
        $notification->delete();
        return redirect()->route('notification.index')->with('successMsg','User Notification Deleted');


    }
}
