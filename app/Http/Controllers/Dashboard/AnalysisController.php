<?php

namespace App\Http\Controllers\Dashboard;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userIndex()
    {
        $numbers_of_users = DB::SELECT("select id, count(*) as count, date(created_at) as date from users WHERE  user_type = 'company' or user_type = 'individual' and date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        return view('dashboard.views.analysis.userIndex',compact('numbers_of_users'));
    }


    public function orderIndex()
    {
        $numbers_of_orders = DB::SELECT("select id, count(*) as count, date(created_at) as date from reservations WHERE  status = '1' and date(created_at) >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY date(created_at)");
        return view('dashboard.views.analysis.orderIndex',compact('numbers_of_orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
