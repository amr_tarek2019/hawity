<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\NotificationRequest;
use App\Notification;
use App\PushNotification;
use App\Reservation;
use App\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notifies=Notification::all();
      // dd($notifies);
         return view('dashboard.views.notification.index',compact('notifies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.notification.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notify($uth_user, $token)
    {
        define('API_ACCESS_KEY', 'AAAA919G5so:APA91bFe-zhOdsSQTIzg5rX5fHcJhxRO_FsQOXrawWAySKWlwOFzV1pbG7FSh1cFqDv1u03XDqTsbZ4-hV5jaJIuIQ_K8qX2hwirqpRZAIWFbyuMRGwbhbWLHejOV70_SJEYE_Pt7LH3');
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
            $notification = [
                'title' => 'تغيير حاله الطلب',
                'body' => 'جارى تنفيذ طلبك',
                'type' => "2",
                'icon' => 'myIcon',
                'sound' => 'mySound'
            ];
            $users= User::where('user_status','1')->pluck('id')->toArray();
        foreach ($users as $users) {

            $notification_table = Notification::create([
                'user_id' => $users,
//                'title' => $request->title,
//                'notification' => $request->notification,
                'title' => $uth_user->title,
                'notification' => $uth_user->notification,
                'icon'=>'http://localhost/grand/hawity/public_html/public/assets/coming/assets/img/favicons/Group%201188.png'
            ]);
            $notification_table->save();
        }
        return $notification_table;


        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
        $fcmNotification = [
            'to' => $token,
            'notification' => $notification,
            'data' => $notification
        ];
        $headers = [
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;

    }
    function store(Request $request)
    {
//        $editOrders = Reservation::where('id', $id)->first();
//        $editOrders->status = $request->status;
        $token = User::where('user_status', '1')->pluck('firebase_token')->toArray();
        $send_notification = $this->notify($request, $token);

        $send_notification->save();

        return redirect()->route('notification.index')->with('successMsg','Notification Successfully Sended');

    }


//    public function store($uth_user, $token, $status)
//    {
//         //$data=$request->all();
////        $data=$request->validate();
////        $notify=PushNotification::create($data);
////          $tokens=User::where('users_type',$request->type)->select('firebase_token')->pluck('firebase_token')->toArray();
////        $regIdChunk=array_chunk($tokens,900);
////        foreach($regIdChunk as $RegId){
////            PushNotification::send_details($RegId,$data['ar']['notifications_title'],$data['ar']['notifications_desc'],8);
////        }
////        return redirect()->route('notification.index')->with('success','Notification Added Successfully');
//
////        $data = $request->all();
////        $userId = Auth::id();
////
////        if(array_key_exists('device_type', $data))
////        {
////            $FCMTokenData = ['user_id' => $userId,
////                'apns_id' => $data['token']];
////            // Add the IOS device token to dbs
////            $FCMTokenData =$this->userFcmToken->create($FCMTokenData);
////        	if ($FCMTokenData)
////            {
////                return response()->json(['message' => 'Token added successfully','data'=>[$FCMTokenData ]],200);
////            }
////            else
////            {
////                return response()->json(['error' => 'Something went wrong!!'], 400);
////            }
////    	}
////        else
////        {
////            $FCMTokenData = ['user_id' => $userId,
////                'token' => $data['token']];
////            // Add the android device token to dbs
////            $FCMTokenData = $this->userFcmToken->create($FCMTokenData);
////        	if ($FCMTokenData)
////            {
////                return response()->json(['message' => 'Token added successfully','data'=>[$FCMTokenData ]],200);
////            }
////            else
////            {
////                return response()->json(['error' => 'Something went wrong!!'], 400);;
////            }
////   	 }
////        $data = $request->all();
////        $userId = Auth::id();
////
////        if(array_key_exists('device_type', $data))
////        {
////            $FCMTokenData = ['user_id' => $userId,
////                'apns_id' => $data['token']];
////            // Add the IOS device token to dbs
////            $FCMTokenData =$this->userFcmToken->create($FCMTokenData);
////        	if ($FCMTokenData)
////            {
////                return response()->json(['message' => 'Token added successfully','data'=>[$FCMTokenData ]],200);
////            }
////            else
////            {
////                return response()->json(['error' => 'Something went wrong!!'], 400);
////            }
////    	}
////        else
////        {
////            $FCMTokenData = ['user_id' => $userId,
////                'token' => $data['token']];
////            // Add the android device token to dbs
////            $FCMTokenData = $this->userFcmToken->create($FCMTokenData);
////        	if ($FCMTokenData)
////            {
////                return response()->json(['message' => 'Token added successfully','data'=>[$FCMTokenData ]],200);
////            }
////            else
////            {
////                return response()->json(['error' => 'Something went wrong!!'], 400);;
////            }
////   	 }
////        $tokens = [];
////        $apns_ids = [];
////        $responseData = [];
////        $data= $request->all();
////        $users= $data['user_ids'];
////        // for Android
////        if ($FCMTokenData = $this->fcmToken->whereIn('user_id',$users)->where('token','!=',null)->select('token')->get())
////        {
////            foreach ($FCMTokenData as $key => $value)
////            {
////                $tokens[] = $value->token;
////            }
////            define('YOUR_SERVER_KEY', 'AAAAt_tQg_o:APA91bFzv1r5BNn-XHzyE4hsbbg010kiJCIcMwyBnC3TBO31Hsi7RgeXx2ejo7q6eqDDVLhKSBUVd4Eb8LWFSBtdr3jLhGo3BnchN1oqRM3T35NGLFvdf_6UlU76a5jIkCdeT3MmgEf8' );
////            $msg = array(
////                'body'  => 'This is body of notification',
////                'title' => 'Notification',
////                'subtitle' =>'this is a subtitle',
////            );
////            $fields = array
////            (
////                'registration_ids'  => $tokens,
////                'notification'  => $msg
////            );
////            $headers = array
////            (
////                'Authorization: key=' . YOUR_SERVER_KEY,
////                'Content-Type: application/json'
////            );$ch = curl_init();
////            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
////            curl_setopt( $ch,CURLOPT_POST, true );
////            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
////            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
////            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
////            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
////            $result = curl_exec($ch );
////            if ($result === FALSE)
////            {
////                die('FCM Send Error: ' . curl_error($ch));
////            }
////            $result = json_decode($result,true);
////            $responseData['android'] =[
////                "result" =>$result
////            ];
////            curl_close( $ch );
////        }
////
////        dd($request);
////
////
//        define('API_ACCESS_KEY',
//            'AAAAt_tQg_o:APA91bFzv1r5BNn-XHzyE4hsbbg010kiJCIcMwyBnC3TBO31Hsi7RgeXx2ejo7q6eqDDVLhKSBUVd4Eb8LWFSBtdr3jLhGo3BnchN1oqRM3T35NGLFvdf_6UlU76a5jIkCdeT3MmgEf8
//');
//        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
//        if ($status == 3) {
//            $notification = [
//                'title' => 'تغيير حاله الطلب',
//                'body' => 'جارى تنفيذ طلبك',
//                'type' => "2",
//                'icon' => 'myIcon',
//                'sound' => 'mySound'
//            ];
//            $notification_table = PushNotification::create([
//                'user_id' => $uth_user,
//                'title' => 'تغيير حاله الطلب',
//                'message' => 'جارى تنفيذ طلبك',
//                'admin_id' => 1
//            ]);
//        } else if ($status == 4) {
//            $notification = [
//                'title' => 'تغيير حاله الطلب',
//                'body' => 'تم الانتهاء من طلبك',
//                'type' => "2",
//                'icon' => 'myIcon',
//                'sound' => 'mySound'
//            ];
//            $notification_table = PushNotification::create([
//                'user_id' => $uth_user,
//                'title' => 'تغيير حاله الطلب',
//                'message' => 'تم الانتهاء من طلبك',
//                'admin_id' => 1
//            ]);
//        } else {
//            return false;
//        }
//        $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];
//        $fcmNotification = [
//            'to' => $token,
//            'notification' => $notification,
//            'data' => $notification
//        ];
//        $headers = [
//            'Authorization: key=' . API_ACCESS_KEY,
//            'Content-Type: application/json'
//        ];
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
//        $result = curl_exec($ch);
//        curl_close($ch);
//        return $result;
//
//        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notify=PushNotification::find($id);
        return view('dashboard.views.notification.edit',compact('notify'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NotificationRequest $request, PushNotification $notify)
    {
//        $data=$request->validated();
//        $notify->update($data);
//        if(isset($request->notifications_img))
//        {
//            $notify->notifications_img=$data['notifications_img'];
//            $notify->save();
//
//
//        }
//        return redirect()->route('notification.index')->with('success','Notification Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notify= Notification::find($id);
        $notify->delete();
        return redirect()->route('notification.index')->with('success','Notification Deleted Successfully');
    }
}
