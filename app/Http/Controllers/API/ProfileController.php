<?php

namespace App\Http\Controllers\API;

use App\User;
use App\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Validator;


class ProfileController extends BaseController
{
    public function index(Request $request)
    {

        // $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
           $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        City::where('id',$user->city_id)->select('id','name_'. $language . ' as name')->first();
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['city_name'] = city::where('id',$user['city_id'])->select('id','name_'. $language . ' as name')->first();
        // $userProfile = User::where('id',$user->id)->select
        // ('id','name','name_'. $language . ' as name','city_id','','email','phone')->first();
        $response=[
            'message'=>'get data of user successfully',
            'status'=>202,
            'data'=>$data,
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

    }

    public function update(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
//        if (!$request->headers->has('jwt')){
//            return callback_data(401, 'check_jwt');
//        }elseif (!$request->headers->has('lang')){
//            return callback_data(401, 'check_lang');
//        }
        $user = \App\User::where('jwt_token',$jwt)->first();
        //return  $user['id'];
        // $validator = Validator::make($request->all(), [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'city_id'=>'required',
        //     'phone'=>'required',
        //     'image'=>'required|mimes:jpeg,jpg,bmp,png',
        // ]);
        // if ($validator->fails()) {
        //     return $this->sendError('Validation Error.', $validator->errors());
        // }
        $user->name = $request->name;
        $user->email=$request->email;
        $user->city_id=$request->city_id;
        $user->phone=$request->phone;
         $user->image=$request->image;
 
 
        if($user->save())
           {
            $response=[
                'message'=>'Profile data changed successfully',
                'status'=>202,
            ];
        }
        return \Response::json($response,202);
        if ($user){
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
            ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
        
            return $response=[
                'success'=>401,
                'message'=>'this acount submitted before',
            ];
        }
            
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }


    public function updatePassword(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
//        if (!$request->headers->has('jwt')){
//            return callback_data(401, 'check_jwt');
//        }elseif (!$request->headers->has('lang')){
//            return callback_data(401, 'check_lang');
//        }
        //return  $user['id'];
        $validator = Validator::make($request->all(), [
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
//        $user=User::find($request->jwt);
        // $user=User::find($request->id);

        $user->password = $request->password;
        if($user->save()){
               $response=[
            'message'=>'Password changed successfully',
            'status'=>202,
        ];
        return \Response::json($response,202);
        }
       
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }


}
