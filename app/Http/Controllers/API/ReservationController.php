<?php

namespace App\Http\Controllers\API;

use App\OrderDetail;
use App\Reservation;
use App\WasteContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use App\TakeDownContainer;
use App\CompanySeller;
use App\PushNotification;


class ReservationController extends BaseController
{
    public function indexBooking(Request $request)
    {
        // $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
//        if (!$request->headers->has('jwt')){
//            return callback_data(401, 'check_jwt');
//        }elseif (!$request->headers->has('lang')){
//            return callback_data(401, 'check_lang');
//        }
        // $user = \App\User::where('jwt_token',$jwt)->first();
        // //return  $user['id'];
        // $reservations = Reservation::select('id',
        //     'order_number','date_from','total')->where('user_id',$user->id)->where('status',0)->get($request->jwt_token);
        // $response=[
        //     'message'=>'get data of booking successfully',
        //     'status'=>202,
        //     'data'=>$reservations,
        // ];
        // return \Response::json($response,200);
        // if (!$request->headers->has('jwt')){
        //     return response(401, 'check_jwt');
        // }elseif (!$request->headers->has('lang')){
        //     return response(401, 'check_lang');
        // }
          $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->get();
        $reservation_id=Reservation::select('id');
        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
             $res_item['id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('id','icon','company_name_'.$language.' as name')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['order_number'] = $res->order_number;
            $res_item['total'] = $res->total;
               $res_list[] = $res_item;

    }
    //return $res_list;
     

        $response = [
            'message' => 'get data of booking successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function indexHistory(Request $request)
    {
    //       $language = $request->header('lang');
    //     if($language=="en"){
    //          $language = "E";
    //     }else {
    //         $language = "A";
    //     }
    //     $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
    //     $user = \App\User::where('jwt_token', $jwt)->first();
    //     $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->get();
    //     $res_item = [];
    //     $res_list  = [];
    //     foreach ($reservation as $res) {
    //         $res_item['user_id'] = $res->user_id;
    //         $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('id','icon','company_name_'.$language.' as name')->first();
    //         $res_item['date_from'] = $res->date_from;
    //         $res_item['order_number'] = $res->order_number;
    //         $res_item['total'] = $res->total;
    //       $res_list[] = $res_item;

    // }
     

    //     $response = [
    //         'message' => 'get data of booking successfully',
    //         'status' => 202,
    //         'data' => $res_list,
    //     ];
    //     return \Response::json($response, 202);
    //     if (!$request->headers->has('jwt')) {
    //         return response(401, 'check_jwt');
    //     } elseif (!$request->headers->has('lang')) {
    //         return response(401, 'check_lang');
    //     }
    $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

//     public function reserve(Request $request)
//     {
//         $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
//         $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

//         $user = \App\User::where('jwt_token',$jwt)->first();
// //        return  $user['jwt'];
//         $validator = Validator::make($request->all(), [
//             'date_from' => 'required',
//             'days' => 'required',
//             'payment_info'=>'required',
//         ]);
//         if ($validator->fails()) {
//             return $this->sendError('Validation Error.', $validator->errors());
//         }
//         $price = WasteContainer::where('id',$request->container_id)->select('price')->first();
//         $order_number = rand(111111111, 999999999);
//         $date_from =$request->date_from;
//         $days=$request->days;
//         $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));

//         $total=$days*$price['price'];

//         $reservation=Reservation::create(array_merge($request->all(),[
//             'order_number' => $order_number,
//             'date_to'=>$date_to,
//             'container_id'=>$request->container_id,
//             'total'=>$total,
//             'status'=>'0',
//         ]));
//         $orderDetails=New OrderDetail();
//         $orderDetails->order_id=$reservation->id;
//         $orderDetails->container_id=$request->container_id;
//         $orderDetails->user_id=$request->user_id;
//         $orderDetails->save();
//         $response=[
//             'message'=>'Reservation request sent successfully',
//             'status'=>'success',
//         ];
//         return \Response::json($response,200);
//         if (!$request->headers->has('jwt')){
//             return response(401, 'check_jwt');
//         }elseif (!$request->headers->has('lang')){
//             return response(401, 'check_lang');
//         }
//     }

      public function reserve(Request $request)
    {
        // $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
              $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();
        $idCompany = CompanySeller::where('id',$request->company_id)->select('company_name_'.$language.' as company_name')->first();
        $order_number = rand(111111111, 999999999);
        $date_from =$request->date_from;
        $days=$request->days;
        $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));
        $total=$days*$price['price'];
        //$reservation=new Reservation();
         $reservation = Reservation::where('user_id',$user->id)->select
        ('id')->where('status',0)->get();
         
        // dd($user->id);
        if(count($reservation->all())==0)
        {
             
         $reservationResult =   Reservation::create(array_merge($request->all(),[
            'order_number' => $order_number,
            'date_to'=>$date_to,
            'container_id'=>$request->container_id,
            'company_id'=>$request->company_id,
            'total'=>$total,
            'status'=>'0',
        ]));
        
         
        $orderDetails=New OrderDetail();
        $orderDetails->order_id=$reservationResult->id;
        $orderDetails->container_id=$request->container_id;
        $orderDetails->company_id=$request->company_id;
        $orderDetails->user_id=$request->user_id;
        $orderDetails->save();
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>'202',
        ];
        }else{
        $orderDetails=New OrderDetail();
        $orderDetails->order_id=$reservation->all()[0]->id;
        $orderDetails->container_id=$request->container_id;
        $orderDetails->company_id=$request->company_id;
        $orderDetails->user_id=$request->user_id;
        $orderDetails->save();
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>202,
        ];
        }
   
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    public function editBooking(Request $request)
    {
        // $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        // $user = \App\User::where('jwt_token',$jwt)->first();
        // $reservation = Reservation::select
        // ('id','user_id','container_id')->find('user_id', $user->id);
        // $response=[
        //     'message'=>'get data of order successfully',
        //     'status'=>202,
        //     'data'=>$reservation,
        // ];
        // return \Response::json($response,202);
        
                   $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list= $res_item;

    }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    public function destroy(Request $request){
    //     $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
    //   // return  $user['id'];
    //     $reservation =Reservation::find($request->id);
    //     $reservation->delete();
    //     $response=[
    //         'message'=>'Reservation successfully deleted',
    //         'status'=>202,
    //     ];
    //     return \Response::json($response,202);
    //     if (!$request->headers->has('jwt')){
    //         return response(401, 'check_jwt');
    //     }elseif (!$request->headers->has('lang')){
    //         return response(401, 'check_lang');
    //     }
     $reservation =Reservation::find($request->id);
        if (Reservation::destroy($request->id)){

            $response=[
                'message'=>'Order successfully deleted',
                'status'=>202,
            ];
            return \Response::json($response,202);
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>401,
            ];
            return \Response::json($response,404);
        }
    }

    public function update(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token',$jwt)->first();
       // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
        ]);
        if ($validator->fails()) {
            // return $this->sendError('Validation Error.', $validator->errors());
            $response=[
            'message'=>'there is something wrong',
            'status'=>'404',
        ];
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();

        $date_from =$request->date_from;
        $days=$request->days;
        $date_to=date('d-m-Y', strtotime($date_from. ' + '.$days.' days'));

        $total=$days*$price['price'];
        $reservation=Reservation::find($request->id);
        $reservation->date_from = $request->date_from;
        $reservation->days=$request->days;
        $reservation->date_to=$date_to;
        $reservation->total=$total;
        $reservation->status='0';
        $reservation->save();
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>'202',
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }
    
    
    
    
   public function IndexReservation(Request $request)
    {
        // $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 0)->get();
        $reservationId=Reservation::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
            $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_id'] = $res->container_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('id','image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Reservations successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }
        
        
        
        
        //update reserve
        public function confirmReservation(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
         $token = \App\User::select('firebase_token')->where('firebase_token',$jwt)->pluck('firebase_token')->toArray();
         PushNotification::send($token,'done',1);
       // return  $user['id'];
        $validator = Validator::make($request->all(), [
            'date_from' => 'required',
            'days' => 'required',
            'payment_info'=>'required'
        ]);
        if ($validator->fails()) {
            $response=[
            'message'=>'failed to booking',
            'status'=>404,
        ];
        }
        $price = WasteContainer::where('id',$request->container_id)->select('price')->first();

         for($i=0;$i<count($request->id);$i++)
         {
        $date_from =$request->date_from[$i];
        $days=$request->days[$i];
        $date_to=date('d-m-Y', strtotime($date_from[$i]. ' + '.$days[$i].' days'));
        $total=$days*$price['price'];
        $reservation=Reservation::find($request->id[$i]);
        //$reservation->date_from = $request->date_from;
        $reservation->days=$request->days[$i];
        $reservation->date_to=$date_to;
        $reservation->total=$total;
        $reservation->payment_info=$request->payment_info;
        $reservation->status='1';
        $reservation->save();
         }
       
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>202,
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }
    }
    






