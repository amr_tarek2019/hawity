<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\CompanySeller;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;


class CompanySellerController extends BaseController
{
    public function CompanySellerRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'company_name_E' => 'required',
            'company_name_A' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'commercial_register' => 'required',
            'tax_record' => 'required',
            'category_id'=>'required',

        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $companySeller = \App\CompanySeller::create(array_merge($request->all(),[
            'icon'=>'default.png',
            'text_E'=>'default',
            'text_A'=>'default',
            'longitude'=>'0',
            'latitude'=>'0',
            'status' => false,
        ]));
        // return $this->sendResponse($companySeller, 'Company register successfully.');
               $response=[
            'status'=>202,
            'data'=>$companySeller,
            'message'=>'Company register successfully .',

        ];
        return \Response::json($response,202);
    }

    public function index(Request $request)
    {

        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        
        $companySellers=CompanySeller::select('id','icon','company_name_'. $language.' as company_name','longitude','latitude',
            'category_id','text_'. $language . ' as text','status')->where('category_id',$request->category_id)->where('status','1')->get();
        $response=[
            'message'=>'get data of company successfully',
            'status'=>202,
            'data'=>$companySellers,
        ];
        return \Response::json($response,202);
    }
    
     
    public function location(Request $request)
    {
       
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        //$user = \App\User::where('jwt_token', $jwt)->first();
        //return  $user['id'];
        $validator = Validator::make($request->all(), [
            'longitude'=>'required',
            'latitude'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $companyLocation = CompanySeller::find($request->id);
        $companyLocation->longitude = $request->longitude;
        $companyLocation->latitude = $request->latitude;
        $companyLocation->save();

        // $user = User::where('longitude', $request->longitude)->orWhere('latitude', $request->latitude)
        //     ->orWhere('name', $request->name)->orWhere('city_id', $request->city_id)->exists();
        // if ($user) {
        //     return $response = [
        //         'success' => 401,
        //         'message' => 'this account submitted before',
        //     ];
        // } else {
            //$user->save();
          // dd($request);
            $response = [
                'message' => 'Location inserted successfully',
                'status' => 202,
            ];
        // }
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }
}
