<?php

namespace App\Http\Controllers\API;

use App\Suggestion;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;


class SuggestionsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
              $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
//        if (!$request->headers->has('jwt')){
//            return callback_data(401, 'check_jwt');
//        }elseif (!$request->headers->has('lang')){
//            return callback_data(401, 'check_lang');
//        }
        $user = \App\User::where('jwt_token',$jwt)->first();
        //return  $user['id'];

        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'suggestion' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
                // Suggestion::create($request->all())->find($request->jwt_token);

        //Suggestion::create($request->all());
//        return $this->sendResponse(null,'Suggestion sent successfully.');
        $suggestion=new Suggestion();
        $suggestion->user_id=$user->id;
        $suggestion->title = $request->title;
        $suggestion->suggestion = $request->suggestion;
        $suggestion->save();
        $response=[
            'message'=>'Suggestion sent successfully',
            'status'=>202,
        ];
        return \Response::json($response,202);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
