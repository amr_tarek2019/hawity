<?php

namespace App\Http\Controllers\API;

use App\OrderDetail;
use App\Reservation;
use App\WasteContainer;
use App\TakeDownContainer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller as BaseController;

class OrderDetailsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
            // $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
            $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $reservation = Reservation::where('user_id', $user->id)->where('status', 1)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($reservation as $res) {
             $res_item['reservation_id'] = $res->id;
            $res_item['user_id'] = $res->user_id;
            $res_item['container_details'] = WasteContainer::where('id',$res->container_id)->select('image','name_'.$language.' as name','description_'.$language.' as description','price','distance')->first();
             $res_item['company_details'] = \App\CompanySeller::where('id',$res->company_id)->select('company_name_'.$language.' as name')->first();
             $res_item['takedown_container'] = TakeDownContainer::where('id',$res->container_id)->select('photo')->first();
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['days'] = $res->days;
            $res_list[] = $res_item;

    }

        $response = [
            'message' => 'get data of User Order successfully',
            'status' => 202,
            'data' => $res_list,
        ];
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $orderDetail=OrderDetail::find($request->id);
        if (OrderDetail::destroy($request->id)){

            $response=[
                'message'=>'Reservation successfully deleted',
                'status'=>202,
            ];
            return \Response::json($response,202);
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>401,
            ];
            return \Response::json($response,404);
        }
    }





}
