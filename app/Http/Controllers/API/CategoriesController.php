<?php

namespace App\Http\Controllers\API;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;

class CategoriesController extends BaseController
{
    public function index(Request $request)
    {
        $language = $request->header('lang');
        if($language=="en"){
             $language = "E";
        }else {
            $language = "A";
        }
        
        $categories=Category::select('id','name_'.$language. ' as name','image')->get();
        $response=[
            'message'=>'get data of categories successfully',
            'status'=>202,
            'data'=>$categories,
        ];
        return \Response::json($response,202);
    }
}
