<?php

namespace App\Http\Controllers\API;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\PushNotification;


class AuthController extends BaseController
{
    public function login(Request $request)
    {
        if (auth()->attempt(['phone' => $request->input('phone'),
            'password' => $request->input('password')])) {
            $user = auth()->user();
//            $data['id'] = $user->id;
//            $data['jwt_token'] = $user->jwt_token;
            if($user) {
                return response()->json([
                    'status'=>202,
                    'message'=>'تم تسجيل الدخول',
                    'data'=>$user
                ]);
            }
        } else {

            return response()->json([
                'status'=>401,
                'message'=>'تاكد من صحه البيانات'
            ]);
        }
    }

    public function registerCompany(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'email' => 'required|email',
            'commercial_register' => 'required',
            'tax_record' => 'required',
            'city_id' => 'required',
           
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
           return $response=[
                'status'=>401,
                'message'=>'this acount submitted before',
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'user_type' => 'company',
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
             'longitude'=>'0',
            'latitude'=>'0',
             
        ]));
//        return $this->sendResponse($user, 'Company register successfully.');
        $response=[
            'status'=>202,
            'data'=>$user,
            'message'=>'Company register successfully .',

        ];
        return \Response::json($response,202);
    }

    public function registerUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'email' => 'required|email',
            'city_id'=>'required',
            
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>'failed',
                'message'=>'this acount submitted before',
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'user_type' => 'individual',
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'commercial_register'=>'default',
            'tax_record'=>'default',
            'longitude'=>'0',
            'latitude'=>'0',
        ]));
//        return $this->sendResponse($user, 'User register successfully.');
        $response=[
            'status'=>202,
            'data'=>$user,
            'message'=>'Company register successfully .',

        ];
        return \Response::json($response,202);
    }

    public function forgotPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required'
        ]);
        $user = User::where('phone', request('phone'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>'code sent successfully',
                'status'=>202,
                'data' => ['code'=>$user->verify_code],
            ];
            return \Response::json($response,202);

        }else{
            // return \Response::json('phone not found',404);
 $response=[
                'message'=>'phone not found',
                'status'=>404,
            ];
            return \Response::json($response,404);
        }

    }

    public function activateCode(Request $request)
    {
        $this->validate($request,[
            'verify_code' => 'required',
        ]);
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code=null;
            $user->save();
            $response=[
                'message'=>'code activation is exist',
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
            // return \Response::json('code activation not found',404);
             $response=[
                'message'=>'code activation not found',
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    public function resetPassword(Request $request)
    {
        $user=User::find($request->id);
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null&&($request->password==$request->password_confirmation)){
        $user->password=$request->password_confirmation;
        }
        if ($user->save())
        {
            $response=[
                'message'=>'new password changed successfully',
                'status'=>202,
                'data'=>$user,
            ];

            return \Response::json($response,202);

        }else{
           $response=[
                'message'=>'something went wrong',
                'status'=>401,
                'data'=>$user,
            ];

            return \Response::json($response,401);
        }
    }
    
    
    public function location(Request $request)
    {
       
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = \App\User::where('jwt_token', $jwt)->first();
        //return  $user['id'];
        $validator = Validator::make($request->all(), [
            'longitude'=>'required',
            'latitude'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::find($request->user_id);
        $user->longitude = $request->longitude;
        $user->latitude = $request->latitude;
        $user->save();

        // $user = User::where('longitude', $request->longitude)->orWhere('latitude', $request->latitude)
        //     ->orWhere('name', $request->name)->orWhere('city_id', $request->city_id)->exists();
        // if ($user) {
        //     return $response = [
        //         'success' => 401,
        //         'message' => 'this account submitted before',
        //     ];
        // } else {
            //$user->save();
           // dd($request);
            $response = [
                'message' => 'Location inserted successfully',
                'status' => 202,
            ];
        // }
        return \Response::json($response, 202);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }
    
    

}

