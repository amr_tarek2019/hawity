<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WasteContainer extends Model
{
    
    
    protected $fillable=['name_E','name_A', 'company_id', 'price',
        'distance', 'description_E','description_A', 'date', 'month', 'days', 'total','image'];
        
        
    protected $table='waste_containers';

   public function company()
    {
        return $this->belongsTo('App\CompanySeller');
    }
    
    public function takeDownContainers()
    {
        return $this->hasMany('App\TakeDownContainer');
    }

    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }


    
      public function getImageAttribute($value)
    {
        if ($value) {
            return asset($value);
        } else {
            return asset('uploads/container/default.png');
        }
    }
}
