<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privacy extends Model
{
    protected $table='privacy';
    protected $fillable=[ 'icon', 'text_E','text_A'];
    
          public function getIconAttribute($value)
    {
        if ($value) {
            return asset($value);
        } else {
            return asset('images/privacy/default.png');
        }
    }
}
