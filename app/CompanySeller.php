<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanySeller extends Model
{
    protected $fillable=['icon', 'name','company_name_E','company_name_A','text_E','text_A',
        'email', 'phone', 'address','category_id' ,'commercial_register', 'tax_record','longitude','latitude'];
    protected $table='company_sellers';

    public function wasteContainers()
    {
        return $this->hasMany('App\WasteContainer');
    }
    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

        public function getIconAttribute($value)
    {
        if ($value) {
            return asset($value);
        } else {
            return asset('images/company/default.png');
        }
    }
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
